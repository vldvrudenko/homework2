"use strict";

$(document).ready(function () {
    $("#menu").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
            console.log(id);
             console.log(top);
        $('body,html').animate({ scrollTop: top }, 1500);
    });
});


$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() >= document.documentElement.clientHeight) {
            $('#up').css('display', 'block');
        } else {
            $('#up').css('display', 'none');
        };
    });
    $('#up').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 1500);
        return false;
    });
});

$(document).ready(function () {
    $('#switcher').click(function () {
        $('.hot-news').slideToggle();
    });
});